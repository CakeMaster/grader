#include "widget.h"
#include "ui_widget.h"

using namespace std;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    QObject::connect(ui->CALCULATE,SIGNAL(pressed()),
                     this,SLOT(display()));
}

Widget::~Widget()
{
    delete ui;
}

bool CheckQStringIsNumber(QString q)
{
    for (int i = 0; i < q.size(); i++)
        if (! q[i].isDigit())
            return false;
    return true;
}

string ConvertToLetterGrade(double num)
{
    if (num > 92)
        return "A";
    if (num > 90)
        return "A-";
    if (num > 87)
        return "B+";
    if (num > 82)
        return "B";
    if (num > 80)
        return "B-";
    if (num > 77)
        return "C+";
    if (num > 72)
        return "C";
    if (num > 70)
        return "C-";
    if (num > 67)
        return "D+";
    if (num > 62)
        return "D";

    return "F";
}

void Widget::display(){

     double homework;
     double midterm1;
     double midterm2;
     double final;

     // --- Calculate Homework ---
     #define FOLDINGSTART {
     homework = 0;

     QVector<QString> homeworkArray;
     homeworkArray.append(ui->A1->text());
     homeworkArray.append(ui->A2->text());
     homeworkArray.append(ui->A3->text());
     homeworkArray.append(ui->A4->text());
     homeworkArray.append(ui->A5->text());
     homeworkArray.append(ui->A6->text());
     homeworkArray.append(ui->A7->text());
     homeworkArray.append(ui->A8->text());

     int homeworkTotal = 0;
     int homeworkAmount = 7;

     // check for any homework within first 7 missing (invalid state)
     bool extraAssignment = true;
     for (int i = 0; i < 8; i++)
     {
        if (homeworkArray[i].isEmpty())
        {
            if (i == 7)
            extraAssignment = false;
            else
                ;  // invalid state
        }

        if (CheckQStringIsNumber(homeworkArray[i]))
            homeworkTotal += homeworkArray[i].toDouble();
        else
            ; //broke
     }

     // check if 8th homework is present (if yes, calc with 8, else calc with only 7)
     if (extraAssignment)
     {
         homeworkTotal += homeworkArray[7].toDouble();
         homeworkAmount = 8;
     }

     homework = homeworkTotal / homeworkAmount;
     // --- End Calculate Homework ---
     #define FOLDINGEND }

     // --- Calculate Midterm 1 ---
     QString midterm1String = ui->M1->text();
     if (! midterm1String.isEmpty() || CheckQStringIsNumber((midterm1String)))
        midterm1 = midterm1String.toDouble();
     else
         ; // invalid state

     // --- Calculate Midterm 2 ---
     QString midterm2String = ui->M2->text();
     if (! midterm2String.isEmpty() || CheckQStringIsNumber((midterm2String)))
        midterm2 = midterm2String.toDouble();
     else
         ; // invalid state

     // --- Calculate Final ---
     QString finalString = ui->F->text();
     if (! finalString.isEmpty() || CheckQStringIsNumber((finalString)))
        final = finalString.toDouble();
     else
         ; // invalid state

     // Schema A - (25% Homework) + (20% Midterm 1) + (20% Midterm 2) + (35% Final Exam)
     double schemaA = (0.25) * homework + (0.2) * midterm1 + (0.2) * midterm2 + (0.35) * final;

     // Schema B - (25% Homework) + (30% Highest Midterm) + (44% Final Exam)
     double schemaB = (0.25) * homework + (0.3) * max(midterm1, midterm2) + (0.35) * final;

     double score = max(schemaA, schemaB);

    ui->OUTPUT->setText(QString::fromStdString("Your final grade in the class as a percentage is: " +
                        to_string(score) +
                        "\nYou will receive a(n): " +
                        ConvertToLetterGrade(score)));

    return;
}

