#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMainWindow>

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

signals:
     void compute();

public slots:
     void display(/*int number*/);

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
